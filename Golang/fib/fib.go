package main

import (
	"fmt"
	"time"
)

func fib(n int) int {
	if n < 2 {
		return n
	}
	return fib(n - 1) + fib(n - 2)
}

func main() {
	start := time.Now()
	for n := 0; n < 40; n++ {
		fmt.Printf("fib (%d) = %d\n", n, fib(n))
	}
	fmt.Printf("Time %s\n", time.Since(start))
}
