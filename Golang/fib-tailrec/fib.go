package main

import (
	"fmt"
	"time"
)

func fib_rec(n int, acc int, piv int) int {
	if n < 1 {
		return acc
	}
	return fib_rec((n - 1), piv, (acc + piv))
}

func fib(n int) int {
	return fib_rec(n, 0, 1)
}

func main() {
	start := time.Now()
	for n := 0; n < 40; n++ {
		fmt.Printf("fib (%d) = %d\n", n, fib(n))
	}
	fmt.Printf("Time %s\n", time.Since(start))
}
