# Turn on tailcall optimization
#RubyVM::InstructionSequence.compile_option = {
#  tailcall_optimization: true,
#  trace_instruction: false,
#}

def fib(x)
  def fib_rec(x, acc, piv)
    case x
    when 0
      acc
    else
      fib_rec(x - 1, piv, acc + piv)
    end
  end
  fib_rec(x, 0, 1)
end

def fib_range(r)
  r.each { |n|
    puts "fib (#{n}): #{fib(n)}"
  }
end

def time(&f)
  start = Time.now
  yield
  stop = Time.now
  puts "Time: #{stop - start}s"
end

if __FILE__ == $0  
  time { fib_range(0..39) }
end
