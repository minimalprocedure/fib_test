def fib(x)
  case x
  when 0
    0
  when 1
    1
  else
    fib(x - 1) + fib(x - 2)
  end
end

def fib_range(r)
  r.each { |n|
    puts "fib (#{n}): #{fib(n)}"
  }
end

def time(&f)
  start = Time.now
  yield
  stop = Time.now
  puts "Time: #{stop - start}s"
end

if __FILE__ == $0
  time { fib_range(0..39) }
end
