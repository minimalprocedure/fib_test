import strutils
import times

proc fib(x: int): int {. noSideEffect .} = 
  case x
  of 0: 0
  of 1: 1
  else: fib(x - 1) + fib(x - 2)

proc fib_range(r: Slice[int]) =
  for i in r:
    echo "fib ($1): $2" % [i.intToStr, fib(i).intToStr]

proc time(f: proc () ) =
  let start = cpuTime()
  f()
  let stop = cpuTime()
  echo "Time: ", stop - start, "s"

time(proc () = fib_range(0..39))
