import strutils
import times

proc fib(n: int, acc: int, piv: int): int =
  if n == 0:
    acc
  else:
    fib(n - 1, piv, acc + piv)
    
proc fib(n: int): int =
  fib(n, 0, 1)

proc fib_range(r: Slice[int]) =
  for i in r:
    echo "fib ($1): $2" % [i.intToStr, fib(i).intToStr]

proc time(f: proc () ) =
  let start = cpuTime()
  f()
  let stop = cpuTime()
  echo "Time: ", stop - start, "s"

time(proc () = fib_range(0..39))
