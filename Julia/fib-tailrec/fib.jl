using Base.Dates

function fib(x)
    function fib_rec(x, acc, piv)
        x == 0 && return acc
        fib_rec((x - 1), piv, (acc + piv))
    end
    fib_rec(x, 0, 1)
end

function fib_range(r)
    for n = r
        r = fib(n)
        println("fib ($n): $r")
    end
end

@time fib_range(0:39)

