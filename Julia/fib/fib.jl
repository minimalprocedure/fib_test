using Base.Dates

function fib(x)
    x == 0 && return 0
    x == 1 && return 1
    fib(x - 1) + fib(x - 2)
end

function fib_range(r)
    for n = r
        r = fib(n)
        println("fib ($n): $r")
    end
end

@time fib_range(0:39)

