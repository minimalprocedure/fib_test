#include <stdio.h>
#include <time.h>
#include <math.h>

int fib_rec(int x, int acc, int piv) {
  switch(x) {
  case 0:
    return acc;
  default:
    return fib_rec((x - 1), piv, (acc + piv));
  }
}

int fib(int x){
  return fib_rec(x, 0, 1);
}

void fib_range(int min, int max) {
  int i;
  for(i = min; i < max; i++) {
    printf("fib (%d): %d\n", i, fib(i));
  }
}

int main() {
  clock_t start;
  clock_t elapsed;
  start = clock();
  fib_range(0, 40); 
  elapsed = clock() - start;
  printf ("Time: %fs\n", ((float)elapsed) / CLOCKS_PER_SEC);
}
