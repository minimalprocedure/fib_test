#include <stdio.h>
#include <time.h>
#include <math.h>

int fib(int x) {
  switch(x) {
  case 0:
    return 0;
  case 1:
    return 1;
  default:
    return fib(x - 1) + fib(x - 2);
  }
}

void fib_range(int min, int max) {
  int i;
  for(i = min; i < max; i++) {
    printf("fib (%d): %d\n", i, fib(i));
  }
}

int main() {
  clock_t start;
  clock_t elapsed;
  start = clock();
  fib_range(0, 40); 
  elapsed = clock() - start;
  printf ("Time: %fs\n", ((float)elapsed) / CLOCKS_PER_SEC);
}
