def fib_rec(x, acc, piv)
  case x
  when 0
    acc
  else
    fib_rec(x - 1, piv, acc + piv)
  end
end

def fib(x)
  fib_rec(x, 0, 1)
end

def fib_range(r)
  r.each { |n|
    puts "fib (#{n}): #{fib(n)}"
  }
end

def time(&f)
  start = Time.now
  f.call
  stop = Time.now
  puts "Time: #{stop - start}s"
end

time { fib_range(0..39) }
