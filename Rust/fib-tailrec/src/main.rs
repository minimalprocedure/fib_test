extern crate time;
use time::PreciseTime;
use std::ops::Range;

fn fib(x: i32) -> i32 {
    fn _fib(x: i32, acc: i32, piv: i32) -> i32 {
        match (x, acc, piv) {
            (0, _, _) => acc,
            _         => _fib(x - 1, piv, acc + piv)
        }
    }    
    _fib(x, 0, 1)
}

fn fib_range(r : Range<i32>) {
    for i in r {
        println!("fib ({:?}) = {:?}", i, fib(i));
    }
}

fn exec_and_time<F>(f : F ) where F: Fn() {
    let start = PreciseTime::now();
    f();
    let stop = PreciseTime::now();
    println!("Time: {}s", start.to(stop));
}

fn main() {
    exec_and_time(||{fib_range(0..40)});
}
