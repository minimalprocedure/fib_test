extern crate time;
use time::PreciseTime;
use std::ops::Range;

fn fib(x : u32) -> u32 {
    match x {
        0 => 0,
        1 => 1,
        _ => fib(x - 1) + fib(x - 2)
    }
}

fn fib_range(r : Range<u32>) {
    for i in r {
        println!("fib ({:?}) = {:?}", i, fib(i));
    }
}

fn exec_and_time<F>(f : F ) where F: Fn() {
    let start = PreciseTime::now();
    f();
    let stop = PreciseTime::now();
    println!("Time: {}s", start.to(stop).num_milliseconds() as f64 / 1000 as f64);
}

fn main() {
    exec_and_time(||{fib_range(0..40)});
}
