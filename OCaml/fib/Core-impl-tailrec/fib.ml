open Core.Std

let time f =
  let start = Time.now () in
  let x = f () in
  let stop = Time.now () in
  printf "Time: %s\n%!" (Time.Span.to_string (Time.diff stop start));
  x
;;

let fib x =
  let rec fib_rec x acc piv =
    match x with
    | 0 -> acc
    | _ -> fib_rec (x - 1) piv (acc + piv)
  in
  fib_rec x 0 1
;;

let range a b =
  let rec aux a b =
    if a > b then [] else a :: aux (a + 1) b  in
  if a > b then List.rev (aux b a) else aux a b
;;

let fib_range range =
  let print_value value =
    let r = fib value in
    fprintf stdout "fib (%d): %d\n" value r;
    flush stdout
  in
  List.iter range print_value
;;

let () =
  time (fun () -> fib_range (range 0 39));
;;
