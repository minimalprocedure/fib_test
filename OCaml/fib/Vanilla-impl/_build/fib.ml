let time f =
  let start = Sys.time () in
  let x = f () in
  let stop = Sys.time () in
  Printf.printf "Time: %fs\n%!" (stop -. start);
  x
;;

let rec fib x =
  match x with
  | 0 -> 0
  | 1 -> 1
  | x -> (fib (x - 1)) + (fib (x - 2))
;;

let range a b =
  let rec aux a b =
    if a > b then [] else a :: aux (a + 1) b  in
  if a > b then List.rev (aux b a) else aux a b
;;

let fib_range range =
  let print_value value =
    let r = fib value in
    Printf.fprintf stdout "fib (%d): %d\n" value r;
    flush stdout
  in
  List.iter print_value range
;;

let () =
  time (fun () -> fib_range (range 0 39));
;;
