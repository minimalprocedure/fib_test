open Parmap

let time f =
  let start = Sys.time () in
  let x = f () in
  let stop = Sys.time () in
  Printf.printf "Time: %fs\n%!" (stop -. start);
  x
;;

let rec fib x =
  match x with
  | 0 -> 0
  | 1 -> 1
  | x -> (fib (x - 1)) + (fib (x - 2))
;;

let fib_range range =
  let print_value value =
    let r = fib value in
    Printf.fprintf stdout "fib (%d): %d\n" value r;
    flush stdout
  in
  Parmap.pariter print_value (L range) ~ncores:4
;;

let () =
  time (fun () -> fib_range (Parmap_utils.range 0 39));
;;
